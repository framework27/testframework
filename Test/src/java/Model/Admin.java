/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Annotation.Url_servlet;
import Utilitaries.ModelView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author USER
 */
public class Admin {
    private String email;
    private String mdp;
    private String username;
    
    private Integer test;
    
    private String nombre_tri=null;

    public int getTest() {
        return test;
    }

    public void setTest(Integer test) {
        this.test = test;
    }

    public String getNombre_tri() {
        return nombre_tri;
    }

    public void setNombre_tri(String nombre_tri) {
        this.nombre_tri = nombre_tri;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Admin() {
    }
    @Url_servlet(url="test")
    public ModelView test_int(){
        ModelView val=new ModelView();
        val.setUrl("test.jsp");
        HashMap<String,Integer> data=new HashMap();
        data.put("nombre", this.getTest());
        val.setData(data);
                
        return val;
    }
    @Url_servlet(url="liste_membre")
    public ModelView liste_membre(){
        ModelView val=new ModelView();
        val.setUrl("membres/liste.jsp");
        
        List<Membre> data=new ArrayList();
        Membre m1=new Membre();
            m1.setId(1);
            m1.setNom("Randrianarisoa");
            m1.setPrenom("Toky");
            m1.setDate_naissance("11/02/2001");
            data.add(m1);
        Membre m2=new Membre();
            m2.setId(2);
            m2.setNom("Rakotondrabe");
            m2.setPrenom("Karen");
            m2.setDate_naissance("02/02/2002");
            data.add(m2);
        Membre m3=new Membre();
            m3.setId(3);
            m3.setNom("Rindrasoa");
            m3.setPrenom("Tantely");
            m3.setDate_naissance("22/12/2002");
            data.add(m3);
        Membre m4=new Membre();
            m4.setId(4);
            m4.setNom("Faniriantsoa");
            m4.setPrenom("Hasina");
            m4.setDate_naissance("24/01/2022");
            data.add(m4);
        HashMap<String,Object> donnee=new HashMap();
        if(nombre_tri!=null){
            int nbr=0;
            try{
                nbr=Integer.valueOf(nombre_tri);
            }catch(Exception e){
                val.setUrl("error.jsp");
            }
            
            if(nbr<data.size() && nbr>0){
    
                data=data.subList(0, nbr);
                               
            }
        }
        donnee.put("liste", data);
        val.setData(donnee);
        return val;
    }
}
